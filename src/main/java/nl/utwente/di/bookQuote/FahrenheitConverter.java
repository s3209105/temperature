package nl.utwente.di.bookQuote;

public class FahrenheitConverter {

    // Convert Fahrenheit to Celsius
    public static double fahrenheitToCelsius(double celsius) {
        return  (celsius * 9f/5) + 32;
    }
}
